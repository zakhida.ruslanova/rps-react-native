import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from "./src/components/pages/HomeScreen";
import CreateSetNameScreen from "./src/components/pages/CreateSetNameScreen";
import JoinGameScreen from "./src/components/pages/JoinGameScreen";
import GameScreen from "./src/components/pages/GameScreen";
import JoinSetNameScreen from "./src/components/pages/JoinSetNameScreen";
const Stack = createStackNavigator();

export default function App() {
  return(
      <NavigationContainer>
          <Stack.Navigator>
              <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Welcome to the game RPS' }}/>
              <Stack.Screen name="CreateSetNameScreen" component={CreateSetNameScreen} />
              <Stack.Screen name="JoinGameScreen" component={JoinGameScreen} />
              <Stack.Screen name="GameScreen" component={GameScreen} />
              <Stack.Screen name="JoinSetNameScreen" component={JoinSetNameScreen} />
          </Stack.Navigator>
      </NavigationContainer>
  );
}