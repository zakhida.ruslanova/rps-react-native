import * as React from 'react';
import {
    Button,
    ImageBackground,
    StyleSheet,
    Text,
    TextInput,
    View,
} from "react-native-web";
import {useEffect} from "react";

const imageRPS = {uri: "https://cdn.hipwallpaper.com/i/23/97/RwMzEj.png"};

export default function CreateSetNameScreen({navigation}) {
    const [name, onChangeText] = React.useState("");
    const [token, setToken] = React.useState("");

    useEffect(() => {
        fetch("http://localhost:8080/auth/token")
            .then((response) => response.text())
            .then((token) => setToken(token))
            .catch((error) => console.error(error));
    }, []);

    function onSubmitHandel() {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json', token: token},
            body: JSON.stringify({name: name})
        };

        fetch('http://localhost:8080/users/name', requestOptions)
            .catch((error) => console.log(error));

        setTimeout(function () {
            fetch('http://localhost:8080/games/start', {
                method: 'GET',
                headers: {'Content-Type': 'application/json', token: token}
            }).then(navigation.navigate('GameScreen', {token: token})).catch((error) => console.log(error));
        }, 1500);
    }

    return (
        <View style={styles.container}>
            <ImageBackground source={imageRPS} style={styles.image}>
                <Text style={styles.text}>Create player</Text>
                <TextInput
                    style={styles.input}
                    placeholder='Type name here...'
                    onChangeText={(val) => onChangeText(val)}
                    value={name}
                />
                <Button
                    style={styles.button}
                    title="Submit"
                    onPress={() => onSubmitHandel()}
                />
            </ImageBackground>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        flex: 1,
        resizeMode: "contain",
        justifyContent: "center",
        width: '100%',
        height: '100%',
    },
    text: {
        color: "white",
        fontSize: 15,
        fontWeight: "bold",
        textAlign: "center",
        marginBottom: 350
    },
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10,
        margin: 20,
        width: 25,
    },
    input: {
        height: 40,
        borderWidth: 1,
        borderColor: '#777',
        padding: 8,
        margin: 10,
        width: 350,
    },
});