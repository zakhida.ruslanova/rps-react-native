import * as React from 'react';
import {Button, FlatList, StyleSheet, Text, View} from "react-native-web";
import {useEffect} from "react";

export default function JoinGameScreen(props) {
    const [games, setGames] = React.useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/games', {
            method: 'GET',
            headers: {'Content-Type': 'application/json', token: props.route.params}
        })
            .then(response => response.json())
            .then(games => {
                setGames(games)
            })
            .catch(error => console.log(error));
    }, []);


    return (
        <View style={styles.container}>
            <Text>JoinGame</Text>
            <FlatList
                data={games}
                keyExtractor={({id}, index) => id}
                renderItem={({item}) => (
                    <Button style={styles.button} title={item.game}
                            onPress={() => props.navigation.navigate('GameScreen', {
                                gameId: item.id,
                                token: props.route.params
                            })}/>
                )}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        width: '100%',
        height: '100%',
    },
    text: {
        fontSize: 25,
        color: "white",
        padding: 100,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10,
        width: 80,
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
});
