import React from 'react';
import {ImageBackground, Button, Text, View, StyleSheet} from "react-native-web";
import CreateSetNameScreen from "./CreateSetNameScreen";

const imageRPS = {uri: "https://cdn.hipwallpaper.com/i/23/97/RwMzEj.png"};

export default function HomeScreen({navigation}) {
    return (
        <View style={styles.container}>
            <ImageBackground source={imageRPS} style={styles.image}>
                <Text style={styles.text}>
                    How to play?
                    The winner is determined by the following rules:
                    - stone beats scissors (stone blunts scissors)
                    - scissors beat paper (scissors cut paper)
                    - paper beats stone (paper wraps stone)
                    - a draw if all players have the same sign at the same time
                    Play until one player remains. He will be the winner.
                </Text>
                <Button
                    style={styles.button}
                    title="Create new game"
                    onPress={() =>
                        navigation.navigate('CreateSetNameScreen')}
                />
                <Button
                    style={styles.button}
                    title="Join game"
                    onPress={() =>
                        navigation.navigate('JoinSetNameScreen')}
                />
            </ImageBackground>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        alignItems: "baseline"
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        width: '100%',
        height: '100%',
    },
    text: {
        color: "white",
        fontSize: 15,
        fontWeight: "bold",
        textAlign: "center",
        marginBottom: 350
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3,
        backgroundColor: 'black',
        padding: 8,
        margin: 10,
        width: 200,
    },
});