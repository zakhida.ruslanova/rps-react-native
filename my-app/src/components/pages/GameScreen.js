import * as React from 'react';
import {Image, ImageBackground, StyleSheet, Text, View} from "react-native-web";
import {useEffect} from "react";
import {TouchableOpacity} from "react-native";

const imageRPS = {uri: "https://cdn.hipwallpaper.com/i/23/97/RwMzEj.png"};
const imageRock = {uri: "https://www.seekpng.com/png/detail/816-8161371_rock-paper-scissor-icon-png.png"};
const imagePaper = {uri: "https://www.vhv.rs/dpng/d/490-4906131_rock-paper-scissors-clipart-rock-paper-scissors-png.png"};
const imageScissors = {uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa5TaN_shjWHbQvO7kRysJje4mWP6Tt-Ihbg&usqp=CAU"};

export default function GameScreen(props) {
    const [playerOneName, setPlayerOneName] = React.useState("");
    const [playerTwoName, setPlayerTwoName] = React.useState("");
    const [gameState, setGameState] = React.useState("");

    function getStatus(token) {
        fetch('http://localhost:8080/games/status', {
            method: 'GET',
            headers: {'Content-Type': 'application/json', token: token}
        })
            .then(response => response.json())
            .then(game => {
                updateTextOnTheScreen(game);
            })
            .catch(error => console.log(error));
    }

    useEffect(() => {
        const token = props.route.params.token;
        if (props.route.params.gameId === '' || props.route.params.gameId === null || props.route.params.gameId === undefined) {
            getStatus(token);
        } else {
            getStatus(token);
            fetch('http://localhost:8080/games/join/' + props.route.params.gameId, {
                method: 'GET',
                headers: {'Content-Type': 'application/json', token: token}
            })
                .then(response => response.json())
                .then(game => {
                    updateTextOnTheScreen(game);
                })
                .catch(error => console.log(error));
        }
    }, []);


    function move(sign) {
        fetch('http://localhost:8080/games/move/' + sign, {
            method: 'GET',
            headers: {'Content-Type': 'application/json', token: props.route.params.token}
        })
            .then(response => response.json())
            .then(game => {
                updateTextOnTheScreen(game);
            })
            .catch(error => console.log(error));
    }

    function updateTextOnTheScreen(game) {
        setPlayerOneName(game.name);
        setPlayerTwoName(game.opponentName);
        setGameState(game.game);
    }

    return (
        <View style={styles.container}>
            <ImageBackground source={imageRPS} style={styles.image}>
                <Text style={styles.text}>GameState: {gameState}</Text>
                <Text style={styles.text}>Player1: {playerOneName}</Text>
                <Text style={styles.text}>Player2: {playerTwoName}</Text>
                <TouchableOpacity activeOpacity={0.5} onPress={() => move('ROCK')}>
                    <Image
                        source={imageRock} style={styles.icon}
                    />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.5} onPress={() => move('PAPER')}>
                    <Image
                        source={imagePaper} style={styles.icon}
                    />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.5} onPress={() => move('SCISSORS')}>
                    <Image
                        source={imageScissors} style={styles.icon}
                    />
                </TouchableOpacity>
            </ImageBackground>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        flex: 1,
        resizeMode: "contain",
        justifyContent: "center",
        width: '100%',
        height: '100%',
    },
    text: {
        color: "white",
        fontSize: 15,
        fontWeight: "bold",
        textAlign: "center",
        marginBottom: 50
    },
    icon: {
        width: 80,
        height: 80,
        padding: 10,
    }
});